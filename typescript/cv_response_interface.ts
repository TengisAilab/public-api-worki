import {EducationLevel, Gender, LanguageType, SkillLevel, SkillType} from "./cv_response_enums";

export interface CvDTO {
    about: About;
    achievements: Achievement[];
    applicantId: number | null;
    cvFileUrl: null | string;
    educations: Education[];
    experiences: Experience[];
    general: General;
    interest: Interest;
    languageType: null | LanguageType;
    otherInformation: OtherInformation;
    ownerStatus: OwnerStatus;
    qualifications: Qualification[];
    references: Reference[];
    skills: Skill[];
    trainings: Training[];
}

export interface About {
    summary?: null | string;
}

export interface Achievement {
    certificateImageUrl?: null | string;
    date?: null | string;
    description?: null | string;
    name?: null | string;
    providedInstitute?: null | string;
}

export interface Education {
    certificateImageBackUrl?: null | string;
    certificateImageFrontUrl?: null | string;
    certificateNumber?: null | string;
    city?: null | string; // ID эсвэл Custom (User-Inputted) city value
    cityValue?: null | string; // City name
    countryId?: number | null;
    countryValue?: null | string; // Country name
    district?: null; // Ашигладаггүй.
    endDate?: null | string;
    gpa?: number | null;
    level?: null | EducationLevel;
    profession?: null | string;
    professionId?: number | null;
    school?: null | string; // ID эсвэл Custom (User-inputted) school name
    schoolValue?: null | string; // School name
    startDate?: null | string;
}

export interface Experience {
    companyId?: number | null;
    companyName?: null | string;
    endDate?: null | string;
    isCurrentWork?: boolean | null;
    position?: null | string;
    positionId?: number | null;
    responsibility?: null | string;
    startDate?: null | string;
}

export interface General {
    birthDate?: null | string;
    contactInfo?: ContactInfo;
    firstName?: null | string;
    gender?: null | Gender;
    lastName?: null | string;
    profileUrl?: null | string;
    regnum?: null | string;
}

export interface ContactInfo {
    contactPhone: string;
    email: string;
    phone: string;
}

export interface Interest {
    interests?: Array<null | string>;
}

export interface OtherInformation {
    cvId?: number | null;
    other?: Object | null;
}

export interface OwnerStatus {
    isCanWorkToCountry?: boolean | null;
    isDisability?: boolean | null;
    isStudent?: boolean | null;
}

export interface Qualification {
    certificateImageUrl?: null | string;
    city?: null | string; // ID эсвэл Custom (User-Inputted) city value
    cityValue?: null | string; // City name
    countryId?: number | null;
    countryValue?: null | string; // Country name
    description?: null | string;
    level?: null | string;
    providedInstitute?: null | string;
    qualification?: null | string;
    qualificationId?: number | null;
    startDate?: null | string;
    endDate?: null | string;
}

export interface Reference {
    company?: null | string; // Name of company
    email?: null | string;
    fileUrl?: null | string;
    firstName?: null | string;
    lastName?: null | string;
    phone?: null | string;
    position?: null | string; // Name of position
}

export interface Skill {
    description?: null | string;
    langCertificateUrl?: null | string;
    level?: null | SkillLevel;
    name?: null | string;
    skillId?: null; // Ашигладаггүй.
    type?: null | SkillType;
}

export interface Training {
    certificateImageUrl?: null | string;
    city?: null | string; // ID эсвэл Custom (User-Inputted) city value
    cityValue?: null | string; // City name
    countryId?: number | null;
    countryValue?: null | string; // Country name
    endDate?: null | string;
    institution?: null | string;
    name?: null | string;
    startDate?: null | string;
}
