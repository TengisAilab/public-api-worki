// languageType
export enum LanguageType {
    "MN", // default
    "EN"
}

// general.gender
export enum Gender {
    "OTHER",
    "MALE",
    "FEMALE"
}

// educations.level
export enum EducationLevel {
    "MIDDLE_SCHOOL",
    "HIGH_SCHOOL",
    "TVET",
    "COLLEGE",
    "BACHELOR",
    "MASTER",
    "DOCTOR"
}

// skills.level
export enum SkillLevel {
    "BEGINNER",
    "INTERMEDIATE",
    "ADVANCED",
    "EXPERT",
    "MASTER",
    "PRO",
}

// skills.type
export enum SkillType {
    "SOFT",
    "HARD",
    "LANGUAGE",
    "TECHNICAL"
}

// otherInformation.other.currentPosition
export enum CurrentPosition {
    "MANAGEMENT",
    "SENIOR",
    "MIDDLE",
    "JUNIOR",
    "INTERN",
    "INDEPENDENT_CONTRACTOR"
}