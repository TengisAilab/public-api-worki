package dto;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class CvDTO implements Serializable {
    private Long id;
    private Long applicantId;
    private LanguageType languageType;
    private GeneralDTO general;
    private AboutDTO about;
    private InterestDTO interest;
    private List<EducationDTO> educations;
    private List<SkillDTO> skills;
    private List<QualificationDTO> qualifications;
    private List<TrainingDTO> trainings;
    private List<AchievementDTO> achievements;
    private List<ExperienceDTO> experiences;
    private List<ReferenceDTO> references;
    private OtherInformationDTO otherInformation;
    private OwnerStatusDTO ownerStatus;
    private String cvFileUrl;

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (super.equals(obj)) {
            return true;
        } else if (Objects.isNull(obj)) {
            return false;
        } else {
            return Objects.nonNull(this.getId()) && this.getId().equals(((CvDTO) obj).getId());
        }
    }
}
