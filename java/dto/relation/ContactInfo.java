package dto;

public class ContactInfo {
    private String phone;
    /**
     * Яаралтай үед холбоо барих хүний мэдээлэл
     */
    private String contactPhone;
    private String email;
}
