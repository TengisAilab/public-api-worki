package dto;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

public class AchievementDTO {
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date date;
    private String name;
    private String providedInstitute;
    private String description;
    private String certificateImageUrl;

}
