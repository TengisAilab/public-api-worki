package dto;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

public class ExperienceDTO {
    private Boolean isCurrentWork;
    private String companyName;
    private String position;
    private Long positionId;
    private String responsibility;
    private Long companyId;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endDate;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startDate;
}
