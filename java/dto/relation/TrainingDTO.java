package dto;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

public class TrainingDTO {
    private Long id;
    private Long countryId;
    private String countryValue;
    private String city;
    private String cityValue;
    private String name;
    private String institution;
    private String certificateImageUrl;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endDate;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startDate;
}
