package dto;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

public class QualificationDTO {
    private Long countryId;
    private String countryValue;
    private String city;
    private String cityValue;
    private String providedInstitute;
    private String qualification;
    private Long qualificationId;
    private String certificateImageUrl;
    private String description;
    private String level;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startDate;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endDate;
}
