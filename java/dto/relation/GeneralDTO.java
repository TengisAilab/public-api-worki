package dto;
import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.Size;
import java.util.Date;

public class GeneralDTO {
    @Size(min = 10, max = 10, message
            = "About Me must be between 10 and 200 characters")
    private String regnum;
    private String firstName;
    private String lastName;
    private String profileUrl;
    private Gender gender;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthDate;
    private ContactInfo contactInfo;
}
