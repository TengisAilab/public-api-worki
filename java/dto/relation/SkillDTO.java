package dto;

public class SkillDTO {
    private Long id;
    private SkillLevel level;
    private String description;
    private String type;
    private String name;
    private String langCertificateUrl;
    private Long skillId;
}