package dto;

public class OwnerStatusDTO {
    private Boolean isStudent;
    private Boolean isDisability;
    private Boolean isCanWorkToCountry;
}
