package dto;
import com.fasterxml.jackson.annotation.JsonFormat;

public class EducationDTO {
    private Long countryId;
    private String countryValue;
    private String city;
    private String cityValue;
    private String district;
    private String profession;
    private Long professionId;
    private EducationLevel level;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startDate;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endDate;
    private String certificateNumber;
    private String certificateImageBackUrl;
    private String certificateImageFrontUrl;
    private String school;
    private String schoolValue;
    private Double gpa;
}
