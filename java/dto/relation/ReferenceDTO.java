package dto;

public class ReferenceDTO {
    private String firstName; // reference гаргах хүний нэр
    private String lastName;
    private String company; // байгууллагын нэр
    private String position;
    private String phone;
    private String email;
    private String fileUrl;
}