package dto;

public enum EducationLevel {
    MIDDLE_SCHOOL,
    HIGH_SCHOOL,
    TVETT
    COLLEGE,
    BACHELOR,
    MASTER,
    DOCTOR,
}
