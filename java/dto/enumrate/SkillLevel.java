package dto;


public enum SkillLevel {
    BEGINNER,
    INTERMEDIATE,
    ADVANCED,
    EXPERT,
    MASTER,
    PRO
}
