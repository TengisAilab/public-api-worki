# Worki Public Api Guide

## Оршил

Энэхүү git repository-д Worki-ийн CV Public API-тай холбогдоход хэрэг болох types, lookups-ийг Java, Typescript, JSON-оор хүргэж байна

## Тайлбар

Манай Public CV API-г дуудсанаар ирэх JSON өгөгдлийг ямар загвартай болохыг Java, Typescript хэлүүдээр <i>./java/dto</i> болон <i>./typescript</i> directories-ууд руу орж харна уу.

Мөн жишиг өгөгдлийг cv_response_example.json файлаас харна уу.